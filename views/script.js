
var gBASE_URL = "https://pucci-mart.onrender.com/api";
$(document).ready(function () {
    onPageLoading()
});

function onPageLoading() {
    loadPetList();
    console.log("page is loading")
}

function loadPetList() {
    const vQueryParams = new URLSearchParams();
    vQueryParams.append('_limit', 8);
    vQueryParams.append('_page', 0);
    vQueryParams.append('priceMin', 200);
    vQueryParams.append('priceMax', 300);
    vQueryParams.append('type', 'Cat');
    vQueryParams.append('type', 'Dog');
    $.ajax({
        type: 'get',
        url: gBASE_URL + '/pets?' + vQueryParams.toString(),
        dataType: 'json',
        success: function (res) {
            console.log(res);
            var vDivListPizzaElement = $("#div-pet-list");
            var vInnerHtml = '';
            for (var bIndex = 0; bIndex < res.rows.length; bIndex++) {
                vInnerHtml += displayPetCol(res.rows[bIndex]);
            }
            vDivListPizzaElement.html(vInnerHtml);
        },
        error: function () {
            console.log(error);
        }
    });
}
// hàm display 1 khối pet
function displayPetCol(paramPetObj) {
    var vHtml = `<div class="col">
    <div class="card" style="border: none;">
        <img src="${paramPetObj.imageUrl}" alt="">
        <div class="card-img-overlay h-50 p-4">
            <span class="bg-danger text-white" style="font-weight: 400; font-size: 16px; padding: 11px 5px; border-radius: 50px/50px;">-${paramPetObj.discount}%</span>
        </div>
        <div class="" style="text-align: center; line-height: 15px;">
            <p class="mt-4" style="font-weight: 500; font-size: 20px;">${paramPetObj.name}</p>
            <p style="font-weight: 300; font-size: 16px;">${paramPetObj.description}</p>
            <p class="fw-light">$${paramPetObj.price}<span class="ms-2"><del>$${paramPetObj.promotionPrice}</del></span></p>
        </div>
    </div>
</div>`;
    return vHtml;
}